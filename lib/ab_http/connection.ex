defmodule AbHttp.Connection do
  defstruct conn_module: nil, message_atom: nil, error_atom: nil,
    close_atom: nil, socket: nil, terminate: false, method: nil,
    path: nil, path_parts: nil, version: :"1.1", request_headers: nil,
    cookies: nil, resp_status: 200, resp_headers: [], resp_body: [],
    disconnect: false, custom: %{}

  @type method :: :get | :post

  @type version :: :"1.1"

  @type t :: %__MODULE__{
    conn_module: module,
    message_atom: atom,
    error_atom: atom,
    close_atom: atom,
    socket: any,
    terminate: boolean,
    method: method,
    path: String.t,
    path_parts: [String.t],
    version: version,
    request_headers: %{String.t => String.t},
    cookies: %{String.t => String.t},
    resp_status: pos_integer,
    resp_headers: [{String.t, String.t}],
    resp_body: iodata | {:file, Path.t},
    disconnect: boolean,
    custom: %{atom => any}
  }

  @type cookie_option :: {:domain, String.t}
    | {:expires, DateTime.t}
    | :http_only
    | {:max_age, integer}
    | :partitioned
    | {:path, String.t}
    | :secure
    | {:same_site, :strict | :lax | :none}

  def dummy, do: %__MODULE__{terminate: false}

  @spec terminate(t) :: t
  def terminate(conn), do: %__MODULE__{conn | terminate: true}

  @spec put_status(t, pos_integer) :: t
  def put_status(conn, status), do: %__MODULE__{conn | resp_status: status}

  @spec put_custom(t, atom, any) :: t
  def put_custom(conn, key, value), do: %__MODULE__{conn | custom: Map.put(conn.custom, key, value)}

  def add_header(conn, name, value), do: %__MODULE__{conn | resp_headers: [{name, value} | conn.resp_headers]}

  def replace_response(%__MODULE__{} = conn, resp), do: %__MODULE__{conn | resp_body: resp}

  def append_response(%__MODULE__{resp_body: body_start} = conn, part),
    do: %__MODULE__{conn | resp_body: [body_start, part]}

  @spec json(t, term) :: t
  def json(conn, data) do
    %__MODULE__{conn | resp_body: Jason.encode!(data)}
    |> add_header("Content-Type", "application/json")
  end

  @spec set_cookie(t, String.t, String.t, [cookie_option]) :: t
  def set_cookie(%__MODULE__{resp_headers: headers} = conn, name, value, opts \\ []) do
    value = ["#{name}=#{value}" | Enum.map(opts, fn
      {:domain, domain} -> "Domain=#{domain}"
      {:expires, time} -> "Expires=#{AbHttp.DateTimeUtils.to_http_format(time)}"
      :http_only -> "HttpOnly"
      {:max_age, age} -> "Max-Age=#{age}"
      :partitioned -> "Partitioned"
      {:path, path} -> "Path=#{path}"
      {:same_site, :strict} -> "SameSite=Strict"
      {:same_site, :lax} -> "SameSite=Lax"
      {:same_site, :none} -> "SameSite=None"
      :secure -> "Secure"
    end)]
    |> Enum.join("; ")
    %__MODULE__{conn | resp_headers: [{"Set-Cookie", value} | headers]}
  end

  def send_response(%__MODULE__{} = conn) do
    version = case conn.version do
      :"1.1" -> "1.1"
    end

    case conn.resp_body do
      {:file, path} ->
        with {:ok, %File.Stat{size: size}} <- File.stat(path) do
          headline = "HTTP/#{version} #{conn.resp_status}\r\n"
          headers = [{"Content-Length", Integer.to_string(size)} | conn.resp_headers]
          |> Enum.map(fn {name, value} -> "#{name}: #{value}\r\n" end)
          head = [headline, headers, "\r\n"]
          conn.conn_module.send(conn.socket, head)

          send_file(&conn.conn_module.send/2, conn.socket, File.open!(path))

          reset(conn)
        else
          {:error, _} ->
            conn
            |> replace_response("File not found")
            |> put_status(500)
            |> send_response()
        end

      iodata ->
        headline = "HTTP/#{version} #{conn.resp_status}\r\n"
        headers = [{"Content-Length", Integer.to_string(IO.iodata_length(conn.resp_body))} | conn.resp_headers]
        |> Enum.map(fn {name, value} -> "#{name}: #{value}\r\n" end)
        data = [headline, headers, "\r\n", iodata]
        conn.conn_module.send(conn.socket, data)

        reset(conn)
    end
  end

  def has_response_header(%__MODULE__{resp_headers: headers}, wanted_key) do
    wanted_key = String.downcase(wanted_key)
    Stream.map(headers, fn {key, _value} -> String.downcase(key) end)
    |> Enum.any?(fn key -> key == wanted_key end)
  end

  def disconnect(%__MODULE__{} = conn), do: %__MODULE__{conn | disconnect: true}

  def remote_addr(%__MODULE__{socket: socket}), do: :inet.peername(socket)

  def reset(conn) do
    %__MODULE__{
      conn_module: conn.conn_module,
      message_atom: conn.message_atom,
      error_atom: conn.error_atom,
      close_atom: conn.close_atom,
      socket: conn.socket
    }
  end

  defp send_file(send, socket, fp) do
    case IO.binread(fp, 4096) do
      :eof -> :ok
      {:error, reason} -> {:error, reason}
      part ->
        send.(socket, part)
        send_file(send, socket, fp)
    end
  end
end
