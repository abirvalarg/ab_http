defmodule AbHttp.Router do
  defmacro __using__(_opts) do
    quote do
      use AbHttp.Plug
      import AbHttp.Router
      import AbHttp.Plug.Builder

      Module.register_attribute(__MODULE__, :plugs, accumulate: true)
      Module.register_attribute(__MODULE__, :paths, accumulate: true)
      Module.register_attribute(__MODULE__, :on_errors, accumulate: true)
      @before_compile AbHttp.Router
    end
  end

  defmacro __before_compile__(env) do
    plugs = Module.get_attribute(env.module, :plugs) |> Enum.reverse()

    paths = Module.get_attribute(env.module, :paths)
    |> Enum.reverse()
    |> Enum.map(fn {method, path, func} ->
      path = String.split(path, "/", trim: true)
      |> Enum.map(fn
        ":" <> path -> {String.to_atom(path), [], Elixir}
        path -> path
      end)

      {method, path, func}
    end)

    on_errors = Module.get_attribute(env.module, :on_errors)
    {on_not_found_mod, on_not_found_func} = on_errors[:not_found] || {AbHttp.DefaultController, :not_found}

    after_routing = Module.get_attribute(env.module, :after_routing, nil)

    quote do
      @impl AbHttp.Plug
      def run(conn, opts) do
        conn
        |> AbHttp.Plug.run(opts, unquote(plugs))
        |> route(opts)
        |> unquote(case after_routing do
          nil -> quote do
            case do conn -> conn end
          end
          module -> quote do
            unquote(module).run(opts)
          end
        end)
      end

      unquote Enum.map(paths, fn
        {:scope, base, {module, func}} ->
          params = Enum.filter(base, fn
            {_, _, _} -> true
            _ -> false
          end)
          |> Enum.map(fn {name, _, _} = item -> {to_string(name), item} end)
          quote do
            defp route(%AbHttp.Connection{path_parts: unquote(base) ++ rest} = conn, opts) do
              unquote(if params != [] do
                quote do: opts = Enum.into(unquote(params), opts)
              else
                quote do end
              end)
              conn = %AbHttp.Connection{conn | path_parts: rest}
              unquote(module). unquote(func)(conn, opts)
            end
          end

        {:ws, path, module} ->
          params = Enum.filter(path, fn
            {_, _, _} -> true
            _ -> false
          end)
          |> Enum.map(fn {name, _, _} = item -> {to_string(name), item} end)
          quote do
            defp route(%AbHttp.Connection{path_parts: unquote(path), method: :get} = conn, opts) do
              unquote(if params != [] do
                quote do: opts = Enum.into(unquote(params), opts)
              else
                quote do end
              end)
              AbHttp.WebSocket.handle(conn, opts, unquote(module))
            end
          end

        {method, path, {module, func}} ->
          params = Enum.filter(path, fn
            {_, _, _} -> true
            _ -> false
          end)
          |> Enum.map(fn {name, _, _} = item -> {to_string(name), item} end)
          quote do
            defp route(%AbHttp.Connection{path_parts: unquote(path), method: unquote(method)} = conn, opts) do
              unquote(if params != [] do
                quote do: opts = Enum.into(unquote(params), opts)
              else
                quote do end
              end)
              unquote(module). unquote(func)(conn, opts)
            end
          end
      end)

      defp route(conn, opts), do: apply(unquote(on_not_found_mod), unquote(on_not_found_func), [conn, opts])
    end
  end

  defmacro scope(path, {:__aliases__, _, _} = module) do
    quote do
      @paths {:scope, unquote(path), {unquote(module), :run}}
    end
  end

  defmacro scope(path, func) do
    quote do
      @paths {:scope, unquote(path), {__MODULE__, unquote(func)}}
    end
  end

  defmacro get(path, module, function) do
    quote do
      @paths {:get, unquote(path), {unquote(module), unquote(function)}}
    end
  end

  defmacro post(path, module, function) do
    quote do
      @paths {:post, unquote(path), {unquote(module), unquote(function)}}
    end
  end

  defmacro websocket(path, module) do
    quote do
      @paths {:ws, unquote(path), unquote(module)}
    end
  end

  defmacro on_not_found(module, function) do
    quote do
      @on_errors {:not_found, {unquote(module), unquote(function)}}
    end
  end

  defmacro after_routing(do: {:__block__, _, block}) do
    quote do
      defmodule AfterRoutingPlug do
        use AbHttp.Plug.Builder

        unquote(block)
      end

      @after_routing AfterRoutingPlug
    end
  end

  defmacro after_routing(do: single_action) do
    quote do
      defmodule AfterRoutingPlug do
        use AbHttp.Plug.Builder

        unquote(single_action)
      end

      @after_routing AfterRoutingPlug
    end
  end
end
