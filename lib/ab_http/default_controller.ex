defmodule AbHttp.DefaultController do
  import AbHttp.Connection

  def not_found(conn, _opts) do
    conn
    |> put_status(404)
    |> append_response("The requested page is not found")
  end

  def bad_request(conn, _opts) do
    conn
    |> put_status(400)
    |> append_response("The server could not decode your request")
  end

  def internal_server_error(conn, _opts) do
    conn
    |> put_status(500)
    |> append_response("Internal server error")
  end
end
