defmodule AbHttp.Plug do
  alias AbHttp.Connection

  @type opts :: String.t | %{String.t => opts}

  @callback init(opts) :: :ok | {:error, reason :: term}

  @callback run(Connection.t, opts) :: Connection.t

  defmacro __using__(_opts) do
    quote do
      @behaviour AbHttp.Plug

      @impl AbHttp.Plug
      def init(opts) do
        opts
      end

      defoverridable init: 1
    end
  end

  def init([]), do: :ok
  def init([{module, {_, opts}} | rest]) do
    module.init(opts)
    init(rest)
  end
  def init([{_, _} | rest]), do: init(rest)

  def run(conn, _opts, []), do: conn
  def run(conn, opts, [{module, {func, _}} | rest]) do
    conn = apply(module, func, [conn, opts])
    if not conn.terminate, do: run(conn, opts, rest), else: conn
  end
  def run(conn, opts, [{module, func} | rest]) do
    conn = apply(module, func, [conn, opts])
    if not conn.terminate, do: run(conn, opts, rest), else: conn
  end
end
