defmodule AbHttp.Plug.Builder do
  require Logger

  defmacro __using__(_opts) do
    quote do
      use AbHttp.Plug
      import AbHttp.Plug
      import AbHttp.Plug.Builder

      @impl AbHttp.Plug
      def init(_opts), do: init_module_plugs()

      @impl AbHttp.Plug
      def run(conn, opts), do: run_plugs(conn, opts)

      Module.register_attribute(__MODULE__, :plugs, accumulate: true)
      @before_compile AbHttp.Plug.Builder
    end
  end

  defmacro __before_compile__(env) do
    plugs = Module.get_attribute(env.module, :plugs, [])
    |> Enum.reverse()
    quote do
      defp run_plugs(conn, opts), do: AbHttp.Plug.run(conn, opts, unquote(plugs))

      defp init_module_plugs(), do: AbHttp.Plug.init(unquote(plugs))
    end
  end

  defmacro plug(module, opts \\ [])

  defmacro plug({:__aliases__, _, _} = module, opts) do
    quote do
      @plugs {unquote(module), {:run, unquote(opts)}}
    end
  end

  defmacro plug(func, _opts) when is_atom(func) do
    quote do
      @plugs {__MODULE__, unquote(func)}
    end
  end
end
