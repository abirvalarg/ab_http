defmodule AbHttp.ConnectHandler do
  defmodule BadContentLength do
    defexception [:message]
  end

  use GenServer, restart: :temporary
  require Logger
  alias AbHttp.DefaultController
  alias AbHttp.Connection

  @http_ver_re ~r/^HTTP\/(.+)$/
  @supported_versions ["1.1"]
  @supported_methods ["GET", "POST"]

  defstruct conn: nil, endpoint: nil, buffer: "", params: nil,
    content_length: nil, stage: :headers

  def start_link(arg), do: GenServer.start(__MODULE__, arg)

  @impl GenServer
  def init({conn, endpoint}) do
    {:ok, %__MODULE__{conn: conn, endpoint: endpoint}}
  end

  @impl GenServer
  def handle_info({message_atom, socket, message},
      %__MODULE__{conn: %Connection{message_atom: message_atom, socket: socket}, stage: :headers} = state) do
    buffer = state.buffer <> message
    sep = check_headers_separator(buffer)
    if sep do
      [headers, buffer] = String.split(buffer, sep <> sep, parts: 2)
      [headline | headers] = String.split(headers, sep)
      with [method, path, protocol] <- String.split(headline, " ", parts: 3),
          [_, version] <- Regex.run(@http_ver_re, protocol),
          true <- method in @supported_methods and version in @supported_versions do
        method = case method do
          "GET" -> :get
          "POST" -> :post
        end
        version = case version do
          "1.1" -> :"1.1"
        end

        uri = URI.parse(path)
        path = uri.path
        params = (if uri.query, do: URI.decode_query(uri.query), else: %{})

        {headers, cookies, length} = headers
        |> Enum.map(fn pair ->
          [key, value] = String.split(pair, ": ", parts: 2)
          {String.downcase(key), value}
        end)
        |> collect_headers()

        path_parts = String.split(path, "/", trim: true)
        conn = %Connection{state.conn | method: method, path: path,
          path_parts: path_parts, version: version, request_headers: headers,
          cookies: cookies}
        state = %__MODULE__{state | conn: conn, content_length: length,
          params: params, buffer: buffer, stage: :body}

        if body_complete?(state) do
          process_request(state)
        else
          {:noreply, state}
        end
      else
        _ ->
          DefaultController.bad_request(state.conn, [])
          |> Connection.send_response()
          {:stop, :normal, state}
      end
    else
      {:noreply, %__MODULE__{state | buffer: buffer}}
    end
  end

  def handle_info({message_atom, socket, message},
      %__MODULE__{conn: %Connection{message_atom: message_atom, socket: socket}, stage: :body} = state) do
    buffer = state.buffer <> message
    state = %__MODULE__{state | buffer: buffer}
    if body_complete?(state) do
      process_request(state)
    else
      {:noreply, state}
    end
  end

  def handle_info({close_atom, socket}, %__MODULE__{conn: %Connection{close_atom: close_atom, socket: socket}} = state) do
    {:stop, :normal, state}
  end

  @impl GenServer
  def terminate(reason, %__MODULE__{} = state) do
    state.conn.conn_module.close(state.conn.socket)
    reason
  end

  defp process_request(%__MODULE__{} = state) do
    case decode_body(state) do
      :bad_request ->
        DefaultController.bad_request(state.conn, [])
        |> Connection.send_response()
        {:stop, :normal, state}
      {body, buffer} ->
        opts = Map.merge(state.params, body)
        conn = try do
          state.endpoint.run(state.conn, opts)
        rescue
          error ->
            Logger.error(inspect(error) <> "\n" <> Exception.format_stacktrace(__STACKTRACE__))
            DefaultController.internal_server_error(state.conn, [])
        end
        if conn.disconnect do
          {:stop, :normal, state}
        else
          conn = Connection.send_response(conn)
          state = %__MODULE__{state | conn: conn, buffer: buffer, stage: :headers}
          {:noreply, state}
        end
      end
  end

  defp body_complete?(%__MODULE__{content_length: cl, buffer: buffer}) do
    !cl or byte_size(buffer) >= cl
  end

  defp check_headers_separator(message) do
    cond do
      String.contains?(message, "\r\n\r\n") -> "\r\n"
      String.contains?(message, "\n\n") -> "\n"
      true -> nil
    end
  end

  defp collect_headers([]), do: {%{}, %{}, nil}
  defp collect_headers([{"content-length", length} | rest]) do
    {headers, cookies, _} = collect_headers(rest)
    with {cl, ""} <- Integer.parse(length) do
      {headers, cookies, cl}
    else
      _ -> raise %BadContentLength{message: "Bad content length"}
    end
  end
  defp collect_headers([{"cookie", current_cookies} | rest]) do
    {headers, cookies, cl} = collect_headers(rest)
    cookies = String.split(current_cookies, "; ")
    |> Enum.map(fn pair ->
      [name, value] = String.split(pair, "=", parts: 2)
      {name, value}
    end)
    |> Enum.into(cookies)
    {headers, cookies, cl}
  end
  defp collect_headers([{name, value} | rest]) do
    {headers, cookies, cl} = collect_headers(rest)
    {Map.put(headers, name, value), cookies, cl}
  end

  defp decode_body(%__MODULE__{} = state) do
    if state.content_length && state.content_length > 0 do
      body = :binary.part(state.buffer, 0, state.content_length)
      buffer = :binary.part(state.buffer, state.content_length, byte_size(state.buffer) - state.content_length)
      case state.conn.request_headers["content-type"] do
        "text/plain" -> body
        "application/x-www-form-urlencoded" ->
          body
          |> String.split("&")
          |> Enum.map(fn pair ->
            with [name, value] <- String.split(pair, "=", parts: 2) do
              {name, URI.decode_www_form(value)}
            else
              [name] -> {name, nil}
            end
          end)
          |> Enum.into(%{})
        "application/json" ->
          with {:ok, body} <- Jason.decode(body) do
            body
          else
            _ -> :bad_request
          end
        other ->
          Logger.error("Unknown content type #{other}")
          :bad_request
      end
      |> case do
        :bad_request -> :bad_request
        body ->
          {body, buffer}
      end
    else
      {%{}, state.buffer}
    end
  end
end
