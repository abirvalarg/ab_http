defmodule AbHttp.WebSocket do
  alias AbHttp.Connection

  @type frame :: {:text, String.t} | {:binary, binary} | {:ping, binary}
    | {:pong, binary}

  @callback init(conn :: Connection.t, opts :: %{String.t => String.t})
    :: {:ok, state :: term} | {:reject, conn :: Connection.t}

  @callback terminate(state :: term, reason :: term) :: :ok

  @callback handle_frame(frame, state :: term) :: {:noreply, state :: term}
    | {:stop, reason :: term, state :: term} | {:push, frame, state :: term}

  @callback handle_info(info :: term, state :: term)
    :: {:noreply, state :: term} | {:stop, reason :: term, state :: term}
      | {:push, frame, state :: term}

  defmacro __using__(_) do
    quote do
      @behaviour AbHttp.WebSocket

      @impl AbHttp.WebSocket
      def terminate(_, _), do: :ok

      @impl AbHttp.WebSocket
      def handle_info(_, state), do: {:noreply, state}

      defoverridable terminate: 2, handle_info: 2
    end
  end

  def handle(conn, opts, module) do
    conn_type = String.split(conn.request_headers["connection"], ", ")
    |> Enum.map(&String.downcase/1)
    upgrade = conn.request_headers["upgrade"]
    key = conn.request_headers["sec-websocket-key"]
    version = conn.request_headers["sec-websocket-version"]
    if "upgrade" in conn_type and upgrade == "websocket" and version == "13" and key do
      module.init(conn, opts)
      |> case do
        {:ok, state} ->
          accept = :base64.encode(:crypto.hash(:sha, key <> "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"))
          conn
          |> Connection.put_status(101)
          |> Connection.add_header("Upgrade", "websocket")
          |> Connection.add_header("Connection", "Upgrade")
          |> Connection.add_header("Sec-WebSocket-Accept", accept)
          |> Connection.send_response()
          loop(conn, module, state)
        {:reject, conn} -> conn
      end
    else
      AbHttp.DefaultController.not_found(conn, %{})
    end
  end

  defp loop(%Connection{close_atom: close_atom, message_atom: message_atom} = conn,
      module, state, buffer \\ "", frame_data \\ nil) do
    action = receive do
      {^close_atom, _socket} ->
        :stop_immediate
      {^message_atom, _socket, message} ->
        buffer = buffer <> message
        handle_buffer(buffer, state, frame_data, module, conn)
      info ->
        case module.handle_info(info, state) do
          {:noreply, state} -> {:continue, state, buffer, frame_data}
          {:stop, reason, state} ->
            send_frame(conn, {:close, ""})
            :ok = module.terminate(reason, state)
            {:stop, :normal}
          {:push, frame, state} ->
            send_frame(conn, frame)
            {:continue, state, buffer, frame_data}
        end
    end

    case action do
      :stop_immediate ->
        Connection.disconnect(conn)
        |> Connection.disconnect()
      {:stop, :bad_frame} ->
        Connection.disconnect(conn)
        |> Connection.disconnect()
      {:stop, :normal} ->
        Connection.terminate(conn)
        |> Connection.disconnect()
      {:continue, state, buffer, frame_data} ->
        loop(conn, module, state, buffer, frame_data)
    end
  end

  defp handle_buffer(buffer, state, frame_data, module, conn) do
    case decode_frame(buffer) do
      {:error, :no_mask} -> {:stop, :bad_frame}
      :not_yet ->
        {:continue, state, buffer, frame_data}
      {opcode, fin, mask, data, buffer} ->
        data = apply_mask(data, mask)
        if fin do
          if opcode == :continue do
            case frame_data do
              nil -> nil
              {opcode, start} -> {opcode, [start, data]}
            end
          else
            {opcode, data}
          end
          |> case do
            nil -> {:stop, :bad_frame}
            frame ->
              case module.handle_frame(frame, state) do
                {:noreply, state} -> {:continue, state, buffer, nil}
                {:stop, reason, state} ->
                  :ok = module.terminate(reason, state)
                  {:stop, :normal}
                {:push, frame, state} ->
                  send_frame(conn, frame)
                  handle_buffer(buffer, state, frame_data, module, conn)
              end
          end
        else
          case {opcode, frame_data} do
            {:continue, {opcode, start}} -> {:continue, state, buffer, {opcode, [start, data]}}
            {opcode, nil} -> {:continue, state, buffer, {opcode, data}}
            _ -> {:stop, :bad_frame}
          end
        end
    end
  end

  defp send_frame(%Connection{} = conn, {opcode, data}) do
    len = byte_size(data)
    start = <<1 :: 1, 0 :: 3, encode_opcode(opcode) :: 4>>
    frame = start <> cond do
      len <= 125 ->
        <<0 :: 1, len :: 7-integer-unsigned>>
      len <= 2 ** 16 - 1 ->
        <<0 :: 1, 126 :: 7-integer-unsigned, len :: 16-integer-unsigned>>
      true ->
        <<0 :: 1, 127 :: 7-integer-unsigned, len :: 64-integer-unsigned>>
    end <> data
    conn.conn_module.send(conn.socket, frame)
  end

  defp decode_frame(<<fin :: 1-integer-unsigned, _ :: 3,
      opcode :: 4-integer-unsigned, 1 :: 1-integer-unsigned,
      127 :: 7-integer-unsigned, len :: 64-integer-unsigned-big,
      mask :: binary-size(4), data :: binary-size(len)>> <> rest) do
    opcode = decode_opcode(opcode)
    if opcode == :error do
      :error
    else
      {opcode, fin == 1, mask, data, rest}
    end
  end
  defp decode_frame(<<fin :: 1-integer-unsigned, _ :: 3,
      opcode :: 4-integer-unsigned, 1 :: 1-integer-unsigned,
      126 :: 7-integer-unsigned, len :: 16-integer-unsigned-big,
      mask :: binary-size(4), data :: binary-size(len)>> <> rest) do
    opcode = decode_opcode(opcode)
    if opcode == :error do
      :error
    else
      {opcode, fin == 1, mask, data, rest}
    end
  end
  defp decode_frame(<<fin :: 1-integer-unsigned, _ :: 3,
      opcode :: 4-integer-unsigned, 1 :: 1-integer-unsigned,
      len :: 7-integer-unsigned, mask :: binary-size(4),
      data :: binary-size(len)>> <> rest) do
    opcode = decode_opcode(opcode)
    if opcode == :error do
      :error
    else
      {opcode, fin == 1, mask, data, rest}
    end
  end
  defp decode_frame(<<_ :: 8, 0 :: 1-integer-unsigned, _ :: 7>> <> _), do: {:error, :no_mask}
  defp decode_frame(_), do: :not_yet

  defp decode_opcode(0), do: :continue
  defp decode_opcode(1), do: :text
  defp decode_opcode(2), do: :binary
  defp decode_opcode(8), do: :close
  defp decode_opcode(9), do: :ping
  defp decode_opcode(10), do: :pong
  defp decode_opcode(_), do: :error

  defp encode_opcode(:continue), do: 0
  defp encode_opcode(:text), do: 1
  defp encode_opcode(:binary), do: 2
  defp encode_opcode(:close), do: 8
  defp encode_opcode(:ping), do: 9
  defp encode_opcode(:pong), do: 10

  defp apply_mask(data, mask, pos \\ 0)
  defp apply_mask(<<>>, _, _), do: <<>>
  defp apply_mask(<<ch>> <> rest, mask, pos) do
    <<Bitwise.bxor(ch, :binary.at(mask, pos))>> <> apply_mask(rest, mask, rem(pos + 1, 4))
  end
end
