defmodule AbHttp.HandlerSupervisor do
  alias AbHttp.ConnectHandler
  use DynamicSupervisor

  def start_link(arg), do: DynamicSupervisor.start_link(__MODULE__, arg, name: __MODULE__)

  @impl DynamicSupervisor
  def init(arg), do: DynamicSupervisor.init(arg)

  def start_handler(conn, endpoint), do: DynamicSupervisor.start_child(__MODULE__, {ConnectHandler, {conn, endpoint}})
end
