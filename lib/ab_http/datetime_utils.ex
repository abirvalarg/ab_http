defmodule AbHttp.DateTimeUtils do
  @spec to_http_format(DateTime.t) :: String.t
  def to_http_format(%DateTime{} = dt) do
    week_day = case rem(trunc(DateTime.to_unix(dt) / 86_400) + 3, 7) do
      0 -> "Mon"
      1 -> "Tue"
      2 -> "Wed"
      3 -> "Thu"
      4 -> "Fri"
      5 -> "Sat"
      6 -> "Sun"
    end
    month = case dt.month do
      1 -> "Jan"
      2 -> "Feb"
      3 -> "Mar"
      4 -> "Apr"
      5 -> "May"
      6 -> "Jun"
      7 -> "Jul"
      8 -> "Aug"
      9 -> "Sep"
      10 -> "Oct"
      11 -> "Nov"
      12 -> "Dec"
    end
    "#{week_day}, #{String.pad_leading(inspect(dt.day), 2, "0")} #{month} #{String.pad_leading(inspect(dt.year), 4, "0")} #{String.pad_leading(inspect(dt.hour), 2, "0")}:#{String.pad_leading(inspect(dt.minute), 2, "0")}:#{String.pad_leading(inspect(dt.second), 2, "0")} GMT"
  end
end
