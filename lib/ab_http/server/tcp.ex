defmodule AbHttp.Server.Tcp do
  use Task, restart: :permanent
  require Logger
  alias AbHttp.HandlerSupervisor
  alias AbHttp.Connection

  def start_link(arg) do
    port = arg[:port] || 8000
    ip = arg[:ip] || :loopback
    endpoint = arg[:endpoint] || raise "No endpoint provided for TCP server"

    with {:ok, socket} <- :gen_tcp.listen(port, ip: ip, reuseaddr: true,
        active: true, mode: :binary) do
      Logger.info("Started a TCP server. IP: #{inspect(ip)}, port: #{port}")
      Task.start_link(fn -> loop(endpoint, socket) end)
    end
  end

  defp loop(endpoint, socket) do
    with {:ok, client} <- :gen_tcp.accept(socket) do
      conn = %Connection{
        conn_module: :gen_tcp,
        message_atom: :tcp,
        error_atom: :tcp_error,
        close_atom: :tcp_closed,
        socket: client,
      }
      with {:ok, pid} <- HandlerSupervisor.start_handler(conn, endpoint) do
        :gen_tcp.controlling_process(client, pid)
      end
    end
    loop(endpoint, socket)
  end
end
