defmodule AbHttp.MixProject do
  use Mix.Project

  def project do
    [
      app: :ab_http,
      version: "0.2.9",
      elixir: "~> 1.14",
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :crypto]
    ]
  end

  defp deps do
    [
      {:jason, "~> 1.4"}
    ]
  end
end
